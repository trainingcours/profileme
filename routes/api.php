<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/users/{plan}','UserController@getUsers');

Route::get('/user/{id}','UserController@getUser');

Route::get('/user/{user_id}/posts','Admin\PostCrudController@getPosts');

Route::get('/user/{user_id}/services','Admin\ServiceCrudController@getServices');

Route::get('/user/{user_id}/skills','Admin\SkillCrudController@getSkills');

Route::get('/user/{user_id}/portfolio/','Admin\PortfolioCrudController@getPortfolio');

Route::get('/user/{user_id}/availability','Admin\AvailabilityCrudController@getAvailability');

Route::get('/user/{user_id}/testimonials','Admin\TestimonialCrudController@getTestimonials');

Route::get('/user/{user_id}/experiences/','Admin\ExperienceCrudController@getExperiences');

Route::post('/user/{user_id}/messages/add/','Admin\MessageCrudController@addMessage');

Route::post('/user/{user_id}/appointments/add/','Admin\AppointmentCrudController@add');

Route::get('/user/{user_id}/appointments/day/{day}/date/{date}','Admin\AppointmentCrudController@getApp');

Route::get('/user/{user_id}/cv','UserController@getCV');

Route::get('/posts/{id}','Admin\PostCrudController@getPost');

Route::get('/features/','Admin\FeatureCrudController@getFeatures');

//Route::post('auth/subscribe/{type}','CheckoutController@subscribe');
//
//Route::get('/paypal/','CheckoutController@show_paypal');

Route::get('/checkout/{type}/','PayController@subscribe');

Route::get('/checkout/paypal/success','PayController@getExpressCheckoutSuccess');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
