<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin'),'paid'],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    CRUD::resource('service', 'ServiceCrudController');
    CRUD::resource('skill', 'SkillCrudController');
    CRUD::resource('experience', 'ExperienceCrudController');
    CRUD::resource('testimonial', 'TestimonialCrudController');
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('post', 'PostCrudController');
    CRUD::resource('appointment', 'AppointmentCrudController');
    CRUD::resource('message', 'MessageCrudController');
    CRUD::resource('cv', 'CvCrudController');
    CRUD::resource('portfolio', 'PortfolioCrudController');
    CRUD::resource('availability', 'AvailabilityCrudController');
    Crud::resource('feature', 'FeatureCrudController');
    Crud::resource('day', 'DayCrudController');
    CRUD::resource('profile', 'UserCrudController')->name('profile');
    Route::get('payment', 'CheckoutController@show_paypal');
    Route::post('subscribe','CheckoutController@subscribe');
}); // this should be the absolute last line of this file