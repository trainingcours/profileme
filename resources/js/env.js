let settings={
    ip:[
        'http://localhost:8000/',
        'http://192.168.0.118:8000/',
        'http://85.196.141.225/',
        'http://84.252.21.214:8000/'
    ],
    env:'prod',
}
let menu=[
    {
        to:'about',
        name:'About',
        icon:'mdi-information'
    },
    {
        to:'services',
        name:'Services',
        icon:'mdi-toolbox-outline'
    },
    {
        to:'resume',
        name:'Resume',
        icon:'mdi-file-account'
    },
    {
        to:'portfolio',
        name:'Portfolio',
        icon:'mdi-projector-screen'
    },
    {
        to:'blog',
        name:'Blog',
        icon:'mdi-post-outline'
    },
    {
        to:'contact',
        name:'Contact',
        icon:'mdi-phone'
    },
    {
        to:'appointment',
        name:'Appointment',
        icon:'mdi-calendar-clock'
    },
]


function getEnv(){
    if(settings.env == 'dev'){
        return settings.ip[0];
    }else{
        return settings.ip[3];
    }
}
export  {settings, getEnv,menu};