/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import router from './router/router.js'
import mix from './mixins/mixin.js'
import Vue from 'vue'
import Footer from './components/Footer'
import axios from 'axios'
import vuetify from './vuetify/vuetify'
import store from './store/store'
import {getEnv} from './env.js';




axios.defaults.baseURL = getEnv()  ;
require('./bootstrap');

window.Vue = require('vue');
Vue.prototype.$EventBus = new Vue();
Vue.component('app-footer',Footer);



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/App.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));


Vue.mixin(mix);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    store,
    data:()=>({
        drawer:false
    })

});
