import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/Home'
import Pricing from '../components/Pricing'
import Features from '../components/Features'
import Users from '../components/Users'
import Profile from '../components/DefaultTheme/Profile'
import PostInfo from '../components/ThemeManager/PostInfo'
import Blog from '../components/ThemeManager/Blog'
import About from '../components/ThemeManager/About'
import Services from '../components/ThemeManager/Services'
import Skills from '../components/ThemeManager/Skills'
import Portfolio from '../components/ThemeManager/Portfolio'
import Resume from '../components/ThemeManager/Resume'
import Contact from '../components/ThemeManager/Contact'
import Appointment from "../components/ThemeManager/Appointment";
import Checkout from "../components/Checkout";
import App from "../components/ThemeOne/App";
import ThemeManager from "../components/ThemeManager/ThemeManager";
import store from'../store/store.js'




Vue.use(Router)

let router =  new Router({
   // mode:'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component:Home
        },
        {
            path: '/home',
            name: 'Home',
            component:Home
        },
        {
            path: '/theme/',
            name: 'App',
            component:App
        },

        {
            path:'/features',
            name:'Features',
            component:Features
        },
        {
            path:'/payment',
            name:'Checkout',
            component:Checkout
        },

        {
            path:'/pricing',
            name:'Pricing',
            component:Pricing
        },
        {
            path:'/users',
            name:'Users',
            component:Users
        },
        {
            path:'/profile/:id/',
            name:'ThemeManager',
            props:true,
            component:ThemeManager,
            children:[
                {
                    path:'blog/posts/:post_id/',
                    props:true,
                    components:{
                        b:PostInfo
                    },

                },
                {
                    path:'blog/posts/:post_id/:loc',

                    redirect:to=> {
                        const { hash, params, query } = to;
                        if(params.loc){
                            return params.loc;
                        }

                    }

                },
                {
                    path:'/',

                    props:true,
                    components:{
                        b:Services
                    },
                    meta: { paid: true }




                },
                {
                    path:'blog',
                    props:true,
                    components:{
                        b:Blog
                    },
                    meta: { paid: true }
                },
                {
                    path:'services',
                    props:true,
                    components:{
                        b:Services
                    },
                    meta: { paid: true }
                },
                {
                    path:'skills',
                    props:true,
                    components:{
                        b:Skills
                    },
                    meta: { paid: true }
                },
                {
                    path:'resume',
                    props:true,
                    components:{
                        b:Resume
                    },
                    meta: { paid: true }
                },
                {
                    path:'about',
                    props:true,
                    components:{
                        b:About
                    }
                },
                {
                    path:'contact',
                    props:true,
                    components:{
                        b:Contact
                    },
                    meta: { paid: true }
                },
                {
                    path:'appointment',
                    props:true,
                    components:{
                        b:Appointment
                    },
                    meta: { paid: true }
                },
                {
                    path:'portfolio',
                    props:true,
                    components:{
                        b:Portfolio
                    },
                    meta: { paid: true }
                },
            ]
        },
    ]


})
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.paid)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.

        if (!store.getters.getPaid) {

            next({
                path:to.path+'about',

            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})
export default  router;


