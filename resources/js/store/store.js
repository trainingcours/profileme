import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
        theme:0,
        path:null,
        ispaid:false
    },
    mutations: {
        SetTheme (state,theme) {
            state.theme = theme;
        },
        setPath (state,path) {
            state.path = path;
        },
        setPaid(state,paid){
            state.ispaid = paid;
        }

    },
    getters:{
        getTheme:state => {
            return state.theme;
        },
        getPath:state => {
            return state.path;
        },
        getPaid:state=>{
            return state.ispaid;
        }
    },
    actions:{

    }
});
export default store;