<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Profile.Me</title>
        <script src="https://js.stripe.com/v3/"></script>

        <!-- Fonts -->


        <!-- Styles -->
    <style>
        a{
            text-decoration: none;
        }
    </style>

    </head>





    <body>
        <div id="app">
            <v-app light id="inspire">
                <v-navigation-drawer
                        v-model="drawer"

                        app
                        right
                        class="hidden-md-and-up"
                >
                    <v-list dense>
                        <v-list-item
                                to="/"
                        >
                            <v-list-item-content>
                                <v-list-item-title>
                                    Home
                                </v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                        <v-list-item
                                to="/features"
                        >
                            <v-list-item-content>
                                <v-list-item-title>
                                    Features
                                </v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                        <v-list-item
                                to="/pricing"
                        >
                            <v-list-item-content>
                                <v-list-item-title>
                                    Pricing
                                </v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                        <v-list-item
                                to="/users"
                        >
                            <v-list-item-content>
                                <v-list-item-title>
                                    Users
                                </v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                        @if(backpack_user())
                        <v-list-item
                                
                        >
                            <v-list-item-content>
                                <v-list-item-title>
                                    <a href="/admin/dashboard" class="black--text">Dashboard</a>
                                </v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                            @endif
                        @if(!backpack_user())
                            <v-list-item

                            >
                                <v-list-item-content>
                                    <v-list-item-title>
                                        <a href="/admin/login" class="black--text">Login</a>
                                    </v-list-item-title>
                                </v-list-item-content>
                            </v-list-item>
                            <v-list-item

                            >
                                <v-list-item-content>
                                    <v-list-item-title>
                                        <a href="/admin/register" class="black--text">Register</a>
                                    </v-list-item-title>
                                </v-list-item-content>
                            </v-list-item>
                            @endif
                    </v-list>
                </v-navigation-drawer>

                <v-app-bar
                        :clipped-left="$vuetify.breakpoint.lgAndUp"
                        app
                        dark

                >
                    <v-toolbar-title
                            style="width:150px"
                            class="ml-0 pl-4"
                    >

                    <v-toolbar-title >
                        Profile.Me


                    </v-toolbar-title>

                    </v-toolbar-title>
                    <section class="hidden-md-and-down">
                        <v-btn text to="/">Home</v-btn>
                        <v-btn text to="/features">Features</v-btn>
                        <v-btn text to="/pricing">Pricing</v-btn>
                        <v-btn text to="/users">Users</v-btn>
                        @if(backpack_user())
                            <v-btn text>
                                <a href="/admin/dashboard" >Dashboard</a>
                            </v-btn>
                        @endif
                        @if(!backpack_user())
                            <v-btn text>

                                <a class = "white--text" href="/admin/login" >Login</a>

                            </v-btn>
                            <v-btn text>
                                <a  class = "white--text" href="/admin/register">Create your own page </a>
                            </v-btn>
                        @endif
                    </section>
                    <v-spacer></v-spacer>
                    <v-app-bar-nav-icon class="hidden-md-and-up" @click.stop="drawer = !drawer" />
                </v-app-bar>


                <v-content>

                    <router-view > </router-view>
                    <br><br>
                    <app-footer></app-footer>



                </v-content>
            </v-app>
        </div>
    </body>
    <script src="{{asset('/js/app.js')}}"></script>
</html>
