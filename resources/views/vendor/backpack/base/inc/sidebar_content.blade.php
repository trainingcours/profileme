

<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li><a href="{{ backpack_url('profile') }}"><i class="fa fa-user"></i> <span>My Profile</span></a></li>
@if(backpack_user()->plan == 'Paid')

<li><a href='{{ backpack_url('service') }}'><i class="fa fa-cog"></i> <span>Services</span></a></li>
<li><a href='{{ backpack_url('skill') }}'><i class='fa fa-cogs'></i> <span>Skills</span></a></li>
<li><a href='{{ backpack_url('experience') }}'><i class='fa fa-book'></i> <span>Experience</span></a></li>
<li><a href='{{ backpack_url('testimonial') }}'><i class="fa fa-comments"></i> <span>Testimonials</span></a></li>
<li><a href='{{ backpack_url('category') }}'><i class='fa fa-tag'></i> <span>Category</span></a></li>
<li><a href='{{ backpack_url('post') }}'><i class='fa fa-newspaper-o'></i> <span>Posts</span></a></li>
<li><a href='{{ backpack_url('appointment') }}'><i class='fa fa-calendar'></i> <span>Appointment</span></a></li>
<li><a href='{{ backpack_url('message') }}'><i class='fa  fa-send'></i> <span>Messages</span></a></li>
<li><a href='{{ backpack_url('portfolio') }}'><i class='fa fa-user'></i> <span>Portfolio</span></a></li>
<li><a href='{{ backpack_url('availability') }}'><i class='fa fa-clock-o'></i> <span>Availability</span></a></li>
<li><a href='{{ backpack_url('cv') }}'><i class='fa fa-upload'></i> <span>Upload CV</span></a></li>

@endif

@if(backpack_user()->hasRole('admin'))

    <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
   <li class='nav-item'><a class='nav-link' href='{{ backpack_url('feature') }}'><i class='nav-icon fa fa-rocket'></i> Features</a></li>"
<li class="treeview">
    <a href="#"><i class="fa fa-group"></i> <span>Users, Roles, Permissions</span> <i class="fa fa-angle-left pull-right"></i></a>
    <ul class="treeview-menu">
        <li><a href="{{ backpack_url('user') }}"><i class="fa fa-user"></i> <span>Users</span></a></li>
        <li><a href="{{ backpack_url('role') }}"><i class="fa fa-group"></i> <span>Roles</span></a></li>
        <li><a href="{{ backpack_url('permission') }}"><i class="fa fa-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
    @endif
