<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Image;



class ImageUpload extends Model
{
    //
    public static function upload($base,$name,$path='',$ext='.png')
    {
        if (substr($base, 0, 4) == "data") {
            if($path == ''){
                $path = 'images/users/' . backpack_user()->id . '/';
            }

            if (!file_exists(public_path($path))) {
                mkdir(public_path($path), 666, true);
            }
           Image::make($base)->save(public_path($path) . $name.$ext='.png' );
            return $path . $name.$ext ;
        }
    }

}
