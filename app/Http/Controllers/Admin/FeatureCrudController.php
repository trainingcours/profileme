<?php

namespace App\Http\Controllers\Admin;

use App\Models\Feature;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\FeatureRequest as StoreRequest;
use App\Http\Requests\FeatureRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class FeatureCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class FeatureCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Feature');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/feature');
        $this->crud->setEntityNameStrings('feature', 'features');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        $this->crud->addColumn([
            'type'=>'icon',
            'label'=>'Icon',
            'name'=>'icon'

        ]);
        $this->crud->addColumn([
            'type'=>'text',
            'label'=>'Name',
            'name'=>'name'

        ]);
        $this->crud->addColumn([
            'type'=>'text',
            'label'=>'Details',
            'name'=>'details'

        ]);

        $this->crud->addField([
            'type'=>'icon_picker',
            'label'=>'Icon',
            'name'=>'icon'

        ]);
        $this->crud->addField([
            'type'=>'text',
            'label'=>'Name',
            'name'=>'name'

        ]);
        $this->crud->addField([
            'type'=>'textarea',
            'label'=>'Details',
            'name'=>'details'

        ]);


        // add asterisk for fields that are required in FeatureRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here

        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getFeatures(){
        $features = Feature::latest()->get();
        return response()->json([
            "features"=>$features
        ]);
    }
}
