<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Srmklive\PayPal\Services\ExpressCheckout;

class CheckoutController extends Controller
{
    public function __construct(){
        $this->middleware('paid',['except'=>['payment','subscribe']]);
    }
    public function show_paypal(){
        return view('checkout');
    }
    public function subscribe(Request $request){
//        $user =User::where("email",$request->stripeEmail)->first();
        $user = auth('backpack')->user();
        $type = 'stripe';
        $token = $request->stripe_token['id'];
//        $user->updateDefaultPaymentMethodFromStripe();
//        $paymentMethod = $user->defaultPaymentMethod();

        if (!$user->is_subscribed) {
            if ($type == 'stripe') {

                $plan = "plan_FI5W86Vk1kA43Q";
                $user->is_paid = true;
                $user->save();
                $user->newSubscription("main", $plan)->create($token);

                return response()->json([
                    'Status'=>'Subscribed'
                ]);
            } else if ($type = "paypal") {
                $provider = new ExpressCheckout;
                $token = $request->token;
                $startdate = Carbon::now()->toAtomString();
                $data = [];
                $profile_desc = "Payment";

                $data = [
                    'PROFILESTARTDATE' => $startdate,
                    'DESC' => $profile_desc,
                    'BILLINGPERIOD' => 'Month', // Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
                    'BILLINGFREQUENCY' => 1, //
                    'AMT' => 10, // Billing amount for each billing cycle
                    'CURRENCYCODE' => 'USD', // Currency code
                    'TRIALBILLINGPERIOD' => 'Day',  // (Optional) Can be 'Day', 'Week', 'SemiMonth', 'Month', 'Year'
                    'TRIALBILLINGFREQUENCY' => 10, // (Optional) set 12 for monthly, 52 for yearly
                    'TRIALTOTALBILLINGCYCLES' => 1, // (Optional) Change it accordingly
                    'TRIALAMT' => 0, // (Optional) Change it accordingly
                ];

                $response = $provider->createRecurringPaymentsProfile($data, $token);
                $user->is_subscribed = true;

            }
        }else{
            return response()->json([
                'status'=>'Already subscribed'
            ]);
        }


    }

    public function unsubscribe(){
        $user = auth('api')->user();
        $user->subscription('main')->cancelNow();
        $user->is_subscribed = false;
        $user->save();
        return response()->json([
            "status"=>"unsubscribed"
        ]);
    }

    //
}
