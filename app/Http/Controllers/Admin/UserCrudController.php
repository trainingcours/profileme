<?php

namespace App\Http\Controllers\Admin;

use App\ImageUpload;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\UserRequest as StoreRequest;
use App\Http\Requests\UserRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Http\Request;

/**
 * Class UserCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class UserCrudController extends CrudController
{





    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->middleware('paid')->except('/profile') ;
        $this->crud->setModel('App\User');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/profile');
        $this->crud->setEntityNameStrings('user', 'users');
        if(backpack_user()){
            $this->crud->addClause('where', 'id', backpack_user()->id);
        }

        $this->crud->removeButton('create');
        $this->crud->removeButton('delete');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        // $this->crud->setFromDb();
        //Columns
        $this->crud->addColumns([
            [
                'name' => 'profile_image',
                'label' => 'Image',
                'type' => 'image'
            ],
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text'
            ],
            [
                'name' => 'description',
                'label' => 'Description',
                'type' => 'text'
            ],
            [
                'name' => 'designation',
                'label' => 'Designation',
                'type' => 'text'
            ],

        ]);
        //Fields
        $this->crud->addFields([
            [
                "type" => "text",
                "name" => "name",
                "label" => "Name",
                'tab' => 'General',
            ],
            [
                "type" => "text",
                "name" => "designation",
                "label" => "Designation",
                'tab' => 'General',
            ],
            [
                "type" => "image",
                "name" => "profile_image",
                "label" => "Profile Image",
                'tab' => 'General',
            ],
            [
                "type" => "email",
                "name" => "email",
                "label" => "Email Address",
                'tab' => 'General',
            ],
            [
                "type" => "textarea",
                "name" => "description",
                "label" => "Description",
                'tab' => 'General',
            ],
            [
                "type" => "text",
                "name" => "facebook",
                "label" => "Facebook",
                'tab' => 'Social',
            ],
            [
                "type" => "text",
                "name" => "instagram",
                "label" => "Instagram",
                'tab' => 'Social',
            ],
            [
                "type" => "text",
                "name" => "linked_in",
                "label" => "LinkedIn",
                'tab' => 'Social',
            ],
            [
                "type" => "text",
                "name" => "skype",
                "label" => "Skype",
                'tab' => 'Social',
            ],
            [
                "type" => "text",
                "name" => "whats_app",
                "label" => "Whatsapp",
                'tab' => 'Social',
            ],
            [
                "type" => "text",
                "name" => "contact_adress",
                "label" => "Contact Address",
                'tab' => 'Social',
            ],
            [
                "type" => "text",
                "name" => "phone",
                "label" => "Phone Number",
                'tab' => 'Social',
            ],

        ]);
        if(backpack_user()->is_paid){
            $this->crud->addField(  [
                'name' => 'theme',
                'label' => "Theme",
                'type' => 'select_from_array',
                'options' => ["0" => "Classic", "1" => "Material"],
                'allows_null' => false,
                'default' => 'one',
                'tab' => 'General'
            ]);
        }


        // add asterisk for fields that are required in UserRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        if ($request->has('profile_image') && $request->profile_image != null && substr($request->profile_image, 0, 4) != "http") {
            $image = ImageUpload::upload($request->profile_image, 'profile_picture');
            $request["profile_image"] = $image;
        }

        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function subscribe(Request $request)
    {
        $user = backpack_user();
        if (!$user->is_subscribed) {
            $token = $request->stripe_token['id'];

            $plan = "plan_FI5W86Vk1kA43Q";
            $user->is_paid = true;
            $user->save();
            $user->newSubscription("main", $plan)->create($token);
            return response()->json([
                'Status' => 'Subscribed'
            ]);
        }
    }
}
