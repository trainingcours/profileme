<?php

namespace App\Http\Controllers\Admin;

use App\ImageUpload;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\TestimonialRequest as StoreRequest;
use App\Http\Requests\TestimonialRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use function GuzzleHttp\Psr7\str;

/**
 * Class TestimonialCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class TestimonialCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Testimonial');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/testimonial');
        $this->crud->setEntityNameStrings('testimonial', 'testimonials');
        $this->crud->setEntityNameStrings('testimonial', 'testimonials');
        if(backpack_user())
            $this->crud->addClause("where","user_id",backpack_user()->id);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
       // $this->crud->setFromDb();
                //Columns
        $this->crud->addColumn([
            "name"=>"thumb",
            "type"=>"image",
            "label"=>"Thumb"
        ]);
        $this->crud->addColumn([
            "name"=>"name",
            "type"=>"text",
            "label"=>"Name"
        ]);
        $this->crud->addColumn([
            "name"=>"feedback",
            "type"=>"text",
            "label"=>"Feedback"
        ]);

        //Fields

        $this->crud->addField([
           "name"=>"name",
           "type"=>"text",
           "label"=>"Name",
        ]);
        $this->crud->addField([
           "name"=>"feedback",
           "type"=>"textarea",
           "label"=>"Feedback",
        ]);
        $this->crud->addField([
           "name"=>"thumb",
           "type"=>"image",
           "label"=>"Thumb",
        ]);
        // add asterisk for fields that are required in TestimonialRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $name = "testimonials_".backpack_user()->id.str_random();
        $image = ImageUpload::upload($request->thumb,$name);
        $request["thumb"] = $image;
        $request["user_id"]=backpack_user()->id;
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
