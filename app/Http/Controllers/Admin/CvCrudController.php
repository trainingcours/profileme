<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CvRequest as StoreRequest;
use App\Http\Requests\CvRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

/**
 * Class CvCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CvCrudController extends CrudController
{
    public static function boot()
    {
        parent::boot();
        static::deleting(function($obj) {
            \Storage::disk('public')->delete($obj->image);
        });
    }
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Cv');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/cv');
        $this->crud->setEntityNameStrings('cv', 'cvs');
        if(backpack_user()){
            $this->crud->addClause("where","user_id",backpack_user()->id);
        }
        $count = $this->crud->query->get()->count();
        if($count >= 1){
            $this->crud->removeButton('create');
        }


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
       // $this->crud->setFromDb();

        $this->crud->addColumn([
            "type"=>"number",
            'name'=>"id",
            'label'=>'#'
        ]);
        $this->crud->addColumn([
            "type"=>"text",
            'name'=>"location",
            'label'=>'Location'
        ]);
        //Fields
        $this->crud->addField([
            'type'=>"upload",
            'name'=>'location',
            'label'=>'upload',
            'upload' => true,
            'disk' => 'public'
        ]);
        $this->crud->addField([
            'type'=>"hidden",
            'name'=>'user_id',
        ]);



        // add asterisk for fields that are required in CvRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
       

        $request["user_id"]=backpack_user()->id;
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
//    private function upload($cv,$request){
//
//        //$path = 'images/users/' . backpack_user()->id . '/';
//
//
//       return public_path($path).$name;
//    }
}
