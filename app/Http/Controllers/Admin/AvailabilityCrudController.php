<?php

namespace App\Http\Controllers\Admin;

use App\Models\Availability;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AvailabilityRequest as StoreRequest;
use App\Http\Requests\AvailabilityRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\Models\Appointment;

/**
 * Class AvailabilityCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AvailabilityCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Availability');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/availability');
        $this->crud->setEntityNameStrings('availability', 'availabilities');
        $this->crud->setEntityNameStrings('availability', 'availabilities');
        if(backpack_user())
            $this->crud->addClause("where","user_id",backpack_user()->id);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();

        //Columns
        $this->crud->addColumn([
            'label' => "Day", // Table column heading
            'type' => "select",
            'name' => 'day_id', // the column that contains the ID of that connected entity;
            'entity' => 'day', // the method that defines the relationship in your Model
            'attribute' => "day", // foreign key attribute that is shown to user
            'model' => "App\Models\Day", // foreign key model
        ]);//Columns
        $this->crud->addColumn([
            'label' => "Start Hour", // Table column heading
            'type' => "text",
            'name' => 'start_hour', // the column that contains the ID of that connected entity;

        ]);
        $this->crud->addColumn([
            'label' => "End Hour", // Table column heading
            'type' => "text",
            'name' => 'end_hour', // the column that contains the ID of that connected entity;

        ]);
        //Fields
        $this->crud->addField([

            'label' => "Day",
            'type' => 'select2',
            'name' => 'day_id', // the db column for the foreign key
            'entity' => 'day', // the method that defines the relationship in your Model
            'attribute' => 'day', // foreign key attribute that is shown to user
            'model' => "App\Models\Day", // foreign key model

            // optional
            'default' => 2, // set the default value of the select2
        ]);
        $this->crud->addField([
            "type"=>"hidden",
           "name"=>"user_id"
        ]);
        $this->crud->addField([
            "type"=>"time",
           "name"=>"start_hour",
            "label"=>"Start Time"
        ]);
        $this->crud->addField([
            "type"=>"time",
           "name"=>"end_hour",
            "label"=>"End Time"
        ]);


        // add asterisk for fields that are required in AvailabilityRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here

            $request["user_id"]=backpack_user()->id;
            $redirect_location = parent::storeCrud($request);



        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getAvailability($user_id){
        $availability = Availability::where("user_id",$user_id)->with("day")->get();

          return $availability;
    }
}
