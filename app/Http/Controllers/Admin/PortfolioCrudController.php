<?php

namespace App\Http\Controllers\Admin;

use App\Models\Portfolio;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PortfolioRequest as StoreRequest;
use App\Http\Requests\PortfolioRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Illuminate\Support\Facades\Storage;
use App\ImageUpload;

/**
 * Class PortfolioCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PortfolioCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Portfolio');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/portfolio');
        $this->crud->setEntityNameStrings('portfolio', 'portfolios');
       // $this->crud->addClause("where","user_id",backpack_user()->id);
        if(backpack_user())
        $this->crud->addClause("where","user_id",backpack_user()->id);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns

        $this->crud->addColumn([
           'type'=>'image',
           'name'=>'thumb',
           'label'=>'thumb',
        ]);
        $this->crud->addColumn([
           'type'=>'text',
           'name'=>'name',
           'label'=>'Name',
        ])
        ;$this->crud->addColumn([
           'type'=>'text',
           'name'=>'details',
           'label'=>'Details',
        ]);

        //Fields
        $this->crud->addField([
            'name'=>'thumb',
            'label'=>'Thumb',
            'type'=>'image'
        ]);
        $this->crud->addField([
            'name'=>'name',
            'label'=>'Title',
            'type'=>'text'
        ]);
        $this->crud->addField([
            'name'=>'details',
            'label'=>'Details',
            'type'=>'textarea'
        ]);
        $this->crud->addField([
            'name'=>'user_id',
            'type'=>'hidden'
        ]);
        $this->crud->addField([
            'name'=>'status',
            'label'=>'Status',
            'type'=>'select_from_array',
            'options'=>["0"=>"Inactive","1"=>"Active"]
        ]);
        $this->crud->addField([  // Select2
            'label' => "Category",
            'type' => 'select2',
            'name' => 'category_id', // the db column for the foreign key
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Category",

            ]);

        // add asterisk for fields that are required in PortfolioRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {

        $locaiton = 'users/'.backpack_user()->id."/portfolios/";
        $name = str_random();
        // your additional operations before save here
        $image = ImageUpload::upload($request->thumb,$name,$locaiton);
        $request["thumb"] = $image;
        $request["user_id"]= backpack_user()->id;
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $locaiton = 'users/'.backpack_user()->id."/portfolios/";
        $name = str_random();
        // your additional operations before save here
        $image = ImageUpload::upload($request->thumb,$name,$locaiton);
        $request["thumb"] = $image;
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getPortfolio($user_id){
        $user = User::find($user_id);
        $projects  = $user->portfolio()->groupBy('category_id');

        return response()->json([
           "projects"=>$projects,

        ]);
    }

}
