<?php

namespace App\Http\Controllers\Admin;

use App\Models\Service;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ServiceRequest as StoreRequest;
use App\Http\Requests\ServiceRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\ImageUpload;

/**
 * Class ServiceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ServiceCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Service');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/service');
        $this->crud->setEntityNameStrings('service', 'services');
        //$this->crud->addClause('where',"user_id","=", backpack_user()->id);
        if(backpack_user())
        $this->crud->addClause("where","user_id",backpack_user()->id);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //Columns
       $this->crud->addColumn([
           'name'=>'thumb',
           'type'=>'image',
           'label'=>'Thumb'
       ]);
       $this->crud->addColumn([
           'name'=>'name',
           'type'=>'text',
           'label'=>'Name'
       ]);
       $this->crud->addColumn([
           'name'=>'details',
           'type'=>'text',
           'label'=>'Description'
       ]);
       //Fields
        $this->crud->addField([
            'name'=>'name',
            'type'=>'text',
            'label'=>'Name'
        ]);
        $this->crud->addField([
            'name'=>'details',
            'type'=>'textarea',
            'label'=>'Description'
        ]);
        $this->crud->addField([
            'name'=>'thumb',
            'type'=>'image',
            'label'=>'Thumb'
        ]);
        $this->crud->addField([
            'name'=>'user_id',
            'type'=>'hidden',
        ]);

        // add asterisk for fields that are required in ServiceRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        $image = ImageUpload::upload($request->thumb,rand(1,100).backpack_user()->id);
        $request["thumb"]=$image;
        $request["user_id"]=backpack_user()->id;

        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $image = ImageUpload::upload($request->thumb,rand(1,100).backpack_user()->id);
        $request["thumb"]=$image;
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getServices($user_id){
        $services = Service::where("user_id",$user_id)->latest()->get();
        return response()->json([
            "services"=>$services
        ]);
    }
}
