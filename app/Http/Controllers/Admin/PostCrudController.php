<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\Post;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\PostRequest as StoreRequest;
use App\Http\Requests\PostRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\ImageUpload;
use Illuminate\Support\Str;


/**
 * Class PostCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class PostCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Post');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/post');
        $this->crud->setEntityNameStrings('post', 'posts');
        if(backpack_user())
            $this->crud->addClause("where","user_id",backpack_user()->id);


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //$this->crud->setFromDb();
        $this->crud->addColumn([
            "name"=>"thumb",
            "type"=>"image",
            "label"=>"Thumb"
        ]);
        $this->crud->addColumn([
            "name"=>"name",
            "type"=>"text",
            "label"=>"Name"
        ]);
        $this->crud->addColumn([
            "name"=>"details",
            "type"=>"text",
            "label"=>"Details"
        ]);
        $this->crud->addColumn([
            "name"=>"status",
            "type"=>"text",
            "label"=>"Status"
        ]);
        //Fields
        $this->crud->addField([
           'name'=>'user_id',
            'type'=>'hidden'//User id
        ]);
        $this->crud->addField([

            'name'=>'name',
            'type'=>'text',
            'label'=>'Title'//User id
        ]);
        $this->crud->addField([

                'name'=>'thumb',
                'type'=>'image',
                'label'=>'Thumb'//User id
        ]);
        $this->crud->addField([

                'name'=>'details',
                'type'=>'textarea',
                'label'=>'Details'//User id
        ]);
        $this->crud->addField([

                'name'=>'status',
                'type'=>'select_from_array',
                'options'=>["0"=>"Draft","1"=>"Published"],
                'label'=>'Status'//User id
        ]);


        // add asterisk for fields that are required in PostRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        $name = Str::random();
        $image = ImageUpload::upload($request->thumb,$name);
        $request["thumb"]= $image;
        $request["user_id"]=backpack_user()->id;
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $name = Str::random();
        $image = ImageUpload::upload($request->thumb,$name);
        $request["thumb"]= $image;
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getPosts($user_id){
        $posts = Post::where("user_id",$user_id)->with('user')->latest()->get();
        return response()->json([
            "posts"=>$posts
        ]);
    }
    public function getPost($id){
        $post = Post::where('id',$id)->with('user')->get();
        return response()->json([
            "post"=>$post
        ]);
    }
}
