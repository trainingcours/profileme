<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\CategoryRequest as StoreRequest;
use App\Http\Requests\CategoryRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');
        $this->crud->addClause("where","user_id",backpack_user()->id);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
      //  $this->crud->setFromDb();
        //Columns
        $this->crud->addColumn([
            "name"=>"name",
            "type"=>"text",
            "label"=>"Name"
        ]);
        $this->crud->addColumn([
            "name"=>"status",
            "type"=>"text",
            "label"=>"Status"
        ]);

        //Fields
        $this->crud->addField([
            "name"=>"name",
            'type'=>'text',
            'label'=>'Name'
        ]);
        $this->crud->addField([
            "name"=>"status",
            'type'=>'select_from_array',
            'options' => ['1' => 'Active', '0' => 'Inactive'],
            'allows_null' => false,
            'label'=>'Status'
        ]);
        $this->crud->addField([
           "name"=>"is_post_category",
            'type'=>'select_from_array',
            'options'=>['1'=>'Blog','0'=>'Portfolio'],
            'label'=>'Category for'
        ]);
        $this->crud->addField([
           'name'=>'user_id',
           'type'=>'hidden'
        ]);

        // add asterisk for fields that are required in CategoryRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $request["user_id"]= backpack_user()->id;
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
