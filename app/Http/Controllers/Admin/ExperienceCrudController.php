<?php

namespace App\Http\Controllers\Admin;

use App\Models\Experience;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ExperienceRequest as StoreRequest;
use App\Http\Requests\ExperienceRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use App\Models\User;

/**
 * Class ExperienceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ExperienceCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Experience');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/experience');
        $this->crud->setEntityNameStrings('experience', 'experiences');
        if(backpack_user())
        $this->crud->addClause('where','user_id',backpack_user()->id);

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //Columns
        $this->crud->addColumn([
            'name'=>'name',
           'type'=>'text',
            'label'=>'Name'
        ]);
        $this->crud->addColumn([
            'name'=>'order',
            'type'=>'number',
            'label'=>'Order'
        ]);
        $this->crud->addColumn([
            'name'=>'details',
            'type'=>'text',
            'label'=>'Details'
        ]);
        $this->crud->addColumn([
            'name'=>'details',
            'type'=>'text',
            'label'=>'Details'
        ]);
        $this->crud->addColumn([
            'name'=>'color',
            'type'=>'color',
            'label'=>'Color'
        ]);
        $this->crud->addColumn([
            'label' => "Category", // Table column heading
            'type' => "select",
            'name' => 'category_id', // the column that contains the ID of that connected entity;
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => "name", // foreign key attribute that is shown to user
            'model' => "App\Models\Category", // foreign key model
        ]);

//Fields
        $this->crud->addField([
            "type"=>"hidden",
            'name'=>'user_id'
        ]);
        $this->crud->addField([
            "type"=>"text",
            'name'=>'name',
            'label'=>'Name'
        ]);
        $this->crud->addField([
            "type"=>"number",
            'name'=>'order',
            'label'=>'Order'
        ]);
        $this->crud->addField([
            "type"=>"textarea",
            'name'=>'details',
            'label'=>'Details'
        ]);
        $this->crud->addField([
            'name' => 'start_date', // db columns for start_date & end_date
            'label' => 'Start Date',
            'type' => 'date_picker',

        ]);
        $this->crud->addField([
            'name' => 'end_date', // db columns for start_date & end_date
            'label' => 'End Date',
            'type' => 'date_picker',

        ]);
        $this->crud->addField([
            'name' => 'color', // db columns for start_date & end_date
            'label' => 'Color ',
            'type' => 'color_picker',

        ]);

        $this->crud->addField([
            'label' => "Category",
            'type' => 'select2',
            'name' => 'category_id', // the db column for the foreign key
            'entity' => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Category",
        ]);



        // add asterisk for fields that are required in ExperienceRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $request["user_id"] = backpack_user()->id;
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function getExperiences($user_id){
        $user = User::find($user_id);
        $experience = $user->experience()->groupBy('category_id');
        return response()->json([
            "experience"=>$experience
        ]);

    }


}
