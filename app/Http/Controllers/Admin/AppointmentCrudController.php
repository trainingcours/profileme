<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AppointmentRequest;
use App\Models\Appointment;
use App\Models\Availability;
use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\AppointmentRequest as StoreRequest;
use App\Http\Requests\AppointmentRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;
use Carbon\CarbonInterval;
use Carbon\Carbon;

/**
 * Class AppointmentCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AppointmentCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Appointment');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/appointment');
        $this->crud->setEntityNameStrings('appointment', 'appointments');
        $this->crud->setEntityNameStrings('appointment', 'appointments');
        if(backpack_user()){
            $this->crud->addClause("where","user_id",backpack_user()->id);
            $this->crud->orderBy('created_at','DESC');
        }


        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        //Columns
        $this->crud->addColumn([
            'name'=>'id',
            'type'=>'text',
            'label'=>"#"
        ]);
        $this->crud->addColumn([
            'name'=>'title',
            'type'=>'text',
            'label'=>"Title"
        ]);
        $this->crud->addColumn([
            'name'=>'email',
            'type'=>'text',
            'label'=>"Email"
        ]);
        $this->crud->addColumn([
            'name'=>'date',
            'type'=>'text',
            'label'=>"Date"
        ]);
        $this->crud->addColumn([
            'name'=>'time',
            'type'=>'text',
            'label'=>"Time"
        ]);
        //Fields

        // add asterisk for fields that are required in AppointmentRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
    public function add(AppointmentRequest $request)
    {
        $app = new Appointment();
        $app->title = $request->title;
        $app->user_id = $request->user_id;
        $app->date = $request->date;
        $app->email = $request->email;
        $app->time = $request->time;
        $app->day_id = $request->day_id;
        $app->save();
        return response()->json([
            "status"=>"succ"
        ]);
    }
    public function getApp($user_id,$day,$date){
        $av = Availability::where('user_id',$user_id)->where('day_id',$day)->select('start_hour','end_hour')->get();
        if(count($av)> 0){
            $app = Appointment::where('user_id',$user_id)->where('day_id',$day)->where('date',$date)->latest()->select('time')->get();
            $times = $this->filter($av,$app);
            return response()->json([
                    "times"=>$times
            ]);
        }else{
            return response()->json([
               'error'=>'Please select other day'
            ],422);
        }

    }
    private function deleteElement($element, &$array){
        $index = array_search($element, $array);
        if($index !== false){
            //unset($array[$index]);
            array_splice($array,$index,1);
        }
    }
    private  function  filter($av,$app){
        $times= array();
        $st = new Carbon($av[0]->start_hour);
        $en = new Carbon($av[0]->end_hour);
        $start = $st->hour;
        $end = $en->hour;

        for ($i = (int)$start; $i <= (int)$end; $i++) {

          array_push($times,$i);
        }
        for($i = count($times); $i >= 0;$i--){
            for($j = 0; $j < count($app);$j++){
                $time = new Carbon($app[$j]->time);
                $t = $time->hour;
                $this->deleteElement($t,$times);

            }
        }
       return $times;

    }


}

