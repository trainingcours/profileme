<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    //

    public function getUsers($plan){
        $users = "";
        if($plan == "*"){
            $users = User::latest()->get();
        }else{
            $users = User::latest()->get()->where("plan",$plan);
        }

        return response()->json([
            'users'=>$users
        ]);
    }
    public function getUser($id){
      $user = User::where('id',$id)->with('cv')->first();
      $cv='';
      if($user->cv){
          $cv= '/storage/'.$user->cv->location ;
      }




     // $url=Storage::get($cv["location"]);
      return response()->json([
          'user'=>$user,
          'cv'=>$cv
      ]);
    }

}
