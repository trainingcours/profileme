<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Srmklive\PayPal\Services\ExpressCheckout;
use Srmklive\PayPal\Services\AdaptivePayments;
use Carbon\Carbon;

class PayController extends Controller
{
    //


    public function subscribe(Request $request){
        $token='';
        $provider =new ExpressCheckout();

        $cart=  $this->GetData();

        try {
            $response = $provider->setExpressCheckout($cart, true);
            dd($response);
            return redirect($response['paypal_link']);
        } catch (\Exception $e) {

        }

    }

    public function getExpressCheckoutSuccess(Request $request)
    {
        $provider =new ExpressCheckout();
        $token = $request->get('token');
        $PayerID = $request->get('PayerID');
        $cart = $this->GetData();

        // Verify Express Checkout Token
        $response = $provider->getExpressCheckoutDetails($token);
        if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            $response = $this->provider->createMonthlySubscription($response['TOKEN'], 9.99, $cart['subscription_desc']);

        }
        dd($response);
    }
    private function GetData(){
        $data = [];

        $data['items'] = [
            [
                'name'  => "Monthly Subscription",
                'price' => 10,
                'qty'   => 1,
            ],
        ];

        $data['subscription_desc'] = "Monthly Subscription #1";
        $data['invoice_id'] = 1;
        $data['invoice_description'] = "Monthly Subscription #1";
        $data['return_url'] = url('/api/checkout/paypal/success');
        $data['cancel_url'] = url('/');

        $total = 0;
        foreach ($data['items'] as $item) {
            $total += $item['price'] * $item['qty'];
        }

        $data['total'] = $total;
        return $data;
    }
}
