<?php

namespace App\Http\Middleware;

use Closure;

class CheckIfPaid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if($request->is('admin/payment*')
            ||$request->is('admin/subscribe*')
            ||$request->is('admin/profile*')
            ){
            return $next($request);
        }else{
            if(backpack_user()->is_paid == 0 && backpack_user()->plan == "Paid" || backpack_user()->plan == "Free"){
                return redirect('/#payment/');
            }
        }

        return $response;
    }
}
