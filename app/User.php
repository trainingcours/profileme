<?php

namespace App;

use App\Models\Cv;
use App\Models\Experience;
use App\Models\Post;
use App\Models\Service;
use App\Models\Skill;
use App\Models\Testimonial;
use Backpack\CRUD\CrudTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Cashier\Billable;

class User extends Authenticatable
{
    use Notifiable;
    use Billable;
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','plan',
        'designation','description','facebook',
        'twitter','linked_in','instagram',
        'contact_address','skype','whats_app',
        'profile_image','theme'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function testimonials(){
        return $this->hasMany(Testimonial::class);
    }
    public function services(){
        return $this->hasMany(Service::class);
    }
    public function skills(){
        return $this->hasMany(Skill::class);
    }
    public function experiences(){
        return $this->hasMany(Experience::class);
    }
    public function cv(){
        return $this->hasOne(Cv::class);
    }
}
